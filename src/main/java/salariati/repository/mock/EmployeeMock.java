package salariati.repository.mock;

import java.util.ArrayList;
import java.util.List;

import salariati.enumeration.DidacticFunction;

import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeMock implements EmployeeRepositoryInterface {

	private List<Employee> employeeList;
	private EmployeeValidator employeeValidator;
	
	public EmployeeMock() {
		
		employeeValidator = new EmployeeValidator();
		employeeList = new ArrayList<Employee>();
		
		Employee Ionel   = new Employee("Pacuraru", "1234567890876", DidacticFunction.ASISTENT, "2500");
		Employee Mihai   = new Employee("Dumitrescu", "1234567890876", DidacticFunction.LECTURER, "2500");
		Employee Ionela  = new Employee("Ionescu", "1234567890876", DidacticFunction.LECTURER, "2500");
		Employee Mihaela = new Employee("Pacuraru", "1234567890876", DidacticFunction.ASISTENT, "2500");
		Employee Vasile  = new Employee("Georgescu", "1234567890876", DidacticFunction.TEACHER,  "2500");
		Employee Marin   = new Employee("Puscas", "1234567890876", DidacticFunction.TEACHER,  "2500");
		Employee Ioana = new Employee("Ppp","1234567892345",DidacticFunction.ASISTENT,"2000");
		employeeList.add(Ioana);
		employeeList.add( Ionel );
		employeeList.add( Mihai );
		employeeList.add( Ionela );
		employeeList.add( Mihaela );
		employeeList.add( Vasile );
		employeeList.add( Marin );

	}


	
	@Override
	public boolean addEmployee(Employee employee) {
		if ( employeeValidator.isValid(employee)) {
			employeeList.add(employee);
			return true;
		}
		return false;
	}
	
	@Override
	public void deleteEmployee(Employee employee) {
		// TODO Auto-generated method stub
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		int ok=0;
		//new EmployeeMock();
		for (Employee e:employeeList) {
			if (e.getLastName().equals(oldEmployee.getLastName())) {
				ok = 1;
				e.setFunction(newEmployee.getFunction());
				break;
			}
			//employeeList.add(e);
		}
		if (ok == 1){
			System.out.println("Functia angajatului a fost modificata cu succes!");
		}
		else {
			System.out.println("Angajatul cu acest nume nu exista.");
		}
	}

	@Override
	public List<Employee> getEmployeeList() {
		return employeeList;
	}
	@Override
	public List<Employee> sortEmployeeList(List<Employee> employeeList) {

		for (int i = 0; i < employeeList.size(); i++) {
			for (int j = i + 1; j < employeeList.size(); j++) {
				if (Integer.parseInt(employeeList.get(i).getSalary()) < Integer.parseInt(employeeList.get(j).getSalary())) {
					Employee first = employeeList.get(i);
					Employee second = employeeList.get(j);
					employeeList.set(i, second);
					employeeList.set(j, first);
				}
			}
		}

		for (int i = 0; i < employeeList.size(); i++) {
			for (int j = i + 1; j < employeeList.size(); j++) {
				if (Integer.parseInt(employeeList.get(i).getSalary()) == Integer.parseInt(employeeList.get(j).getSalary())) {
					int an_first = Integer.parseInt(employeeList.get(i).getCnp().substring(1, 3));
					int an_second = Integer.parseInt(employeeList.get(j).getCnp().substring(1, 3));
					if (an_first < an_second) {
						Employee first = employeeList.get(i);
						Employee second = employeeList.get(j);
						employeeList.set(i, second);
						employeeList.set(j, first);
					}
				}
			}
		}
		return employeeList;
	}



	@Override
	public List<Employee> getEmployeeByCriteria(String criteria) {
		// TODO Auto-generated method stub
		return null;
	}

}

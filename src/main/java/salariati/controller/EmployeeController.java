package salariati.controller;

import java.util.List;

import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;

public class EmployeeController {
	
	private EmployeeRepositoryInterface employeeRepository;

	public EmployeeController(EmployeeRepositoryInterface employeeRepository) {
		this.employeeRepository = employeeRepository;
	}

	public EmployeeController() {
		employeeRepository= new EmployeeMock();
	}

	public void addEmployee(Employee employee) {
		employeeRepository.addEmployee(employee);
	}
	
	public List<Employee> getEmployeesList() {
		return employeeRepository.getEmployeeList();
	}

	public List<Employee> sortEmployeeList(List<Employee> employeeList) {return employeeRepository.sortEmployeeList(employeeList );}




	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		employeeRepository.modifyEmployee(oldEmployee, newEmployee);
	}

	public void deleteEmployee(Employee employee) {
		employeeRepository.deleteEmployee(employee);
	}


}

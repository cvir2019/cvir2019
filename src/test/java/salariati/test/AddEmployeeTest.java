package salariati.test;

import static org.junit.Assert.*;
import static salariati.enumeration.DidacticFunction.ASISTENT;
import static salariati.enumeration.DidacticFunction.DOCTORAND;
import static salariati.enumeration.DidacticFunction.LECTURER;


import salariati.model.Employee;

import org.junit.Before;
import org.junit.Test;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;

//pentru lab 2
public class AddEmployeeTest {

	private EmployeeRepositoryInterface employeeRepository;
	private EmployeeController controller;
	private EmployeeValidator employeeValidator;
	
	@Before
	public void setUp() {
		employeeRepository = new EmployeeMock();
		controller         = new EmployeeController(employeeRepository);
		employeeValidator  = new EmployeeValidator();
	}
	
	@Test
	public void testRepositoryMock() {
		assertFalse(controller.getEmployeesList().isEmpty());
		assertEquals(7, controller.getEmployeesList().size());
	}
	
	@Test
	public void testAddNewEmployee() {
		Employee newEmployee = new Employee("ValidLastName", "1910509055057", DidacticFunction.ASISTENT, "3000");
		assertTrue(employeeValidator.isValid(newEmployee));
		controller.addEmployee(newEmployee);
		assertEquals(8, controller.getEmployeesList().size());
		assertTrue(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));
	}


	@Test
	public void test1()  {
		Employee employee = new Employee("Costea Violeta ","1234567890123", LECTURER, "2000");
		Employee e1 = new Employee("Catalina ","1234567890123", LECTURER, "2000");
		Employee e2 = new Employee("Popescu Iulia ","1234567890123", ASISTENT, "800");
		controller.addEmployee(employee);
		controller.addEmployee(e1);
		controller.addEmployee(e2);
		assertEquals(7, controller.getEmployeesList().size());
		//to-DO : DE vf daca ob adaugat = obiectul care trabuia sa se adauge
	}

	@Test
	public void test2()  {

		Employee employee = new Employee("Violeta 3","1234567890123", LECTURER, "2000");
		assertFalse(employeeValidator.isValid(employee));
		controller.addEmployee(employee);
	}

	@Test
	public void test3()  {

		Employee employee = new Employee("Violeta","1234567890123456789", LECTURER, "2000");
		assertFalse(employeeValidator.isValid(employee));
		controller.addEmployee(employee);
	}

	@Test
	public void test4() {

		Employee employee = new Employee("Violeta","1234", LECTURER, "2000");
		assertFalse(employeeValidator.isValid(employee));
		controller.addEmployee(employee);
	}


	@Test
	public void test5()  {

		Employee employee = new Employee("Alina Campean","1234567890123", LECTURER, "");
		assertFalse(employeeValidator.isValid(employee));
		controller.addEmployee(employee);
	}

	@Test
	public void test6() {

		Employee employee = new Employee("Alina Campean","1234567890123", LECTURER, "-2");
		assertFalse(employeeValidator.isValid(employee));
		controller.addEmployee(employee);
	}



	@Test
	public void test7() {

		Employee employee = new Employee("Violeta","asdfghjkloiuy", LECTURER, "2000");
		assertFalse(employeeValidator.isValid(employee));
		controller.addEmployee(employee);
	}

	@Test
	public void test8() {

		Employee employee = new Employee("V","1234567890123", ASISTENT, "800");
		assertFalse(employeeValidator.isValid(employee));
		controller.addEmployee(employee);
	}
	@Test
	public void test9() {

		Employee employee = new Employee("Campean Iulia","1234567890123", ASISTENT, "0");
		assertFalse(employeeValidator.isValid(employee));
		controller.addEmployee(employee);
	}

	@Test
	public void test10() {

		Employee employee = new Employee("Violeta","1234567890123", LECTURER, "abcd");
		assertFalse(employeeValidator.isValid(employee));
		controller.addEmployee(employee);
	}

	@Test
	public void test11() {
		Employee employee = new Employee("Niculescu Mara","1234567890123", ASISTENT, "1000");
		controller.addEmployee(employee);
		assertEquals(7, controller.getEmployeesList().size());

	}

	@Test
	public void test12() {

		Employee employee = new Employee("Andreea Mihalescu","1234567890123", DOCTORAND, "abcd");
		assertFalse(employeeValidator.isValid(employee));
		controller.addEmployee(employee);
	}
}

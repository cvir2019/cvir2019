package salariati.test;

import static org.junit.Assert.*;
import static salariati.enumeration.DidacticFunction.*;


import salariati.model.Employee;

import org.junit.Before;
import org.junit.Test;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.implementations.EmployeeImpl;
import salariati.validator.EmployeeValidator;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;

import java.io.ByteArrayOutputStream;
import java.io.Console;
import java.io.PrintStream;


public class ModifyDidacticFunctionTest {

    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeImpl();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }



    @Test
    public void test1ModifyEmployee() {
        Employee newEmployee = new Employee("ValidLastName", "1910509055057", DidacticFunction.ASISTENT, "3000");
        assertTrue(employeeValidator.isValid(newEmployee));
        controller.addEmployee(newEmployee);
        assertEquals(10, controller.getEmployeesList().size());
        assertTrue(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));

        String name="Maricica",newFunc="CONFERENTIAR";

        Employee oldd = new Employee(name,"",DidacticFunction.ASISTENT,"");

        Employee neww = new Employee("","",DidacticFunction.valueOf(newFunc),"");
        controller.modifyEmployee(oldd,neww);
        assertEquals(controller.getEmployeesList().get(0).getFunction(),CONFERENTIAR);

    }


    @Test
    public void test2() {
        Employee newEmployee = new Employee("ValidLastName", "1910509055057", DidacticFunction.ASISTENT, "3000");
        assertTrue(employeeValidator.isValid(newEmployee));
        controller.addEmployee(newEmployee);
        assertEquals(11, controller.getEmployeesList().size());
        assertTrue(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));

        String name="ValidLastName",newFunc="CONFERENTIAR";

        Employee oldd = new Employee(name,"",DidacticFunction.ASISTENT,"");

        Employee neww = new Employee("","",DidacticFunction.valueOf(newFunc),"");
        controller.modifyEmployee(oldd,neww);
        assertEquals(controller.getEmployeesList().get(10).getFunction(),CONFERENTIAR);

    }

    @Test
    public void test3()  {

        Employee employee = new Employee("Violeta","1234567890123", LECTURER, "2000");
        assertTrue(employeeValidator.isValid(employee));
        controller.addEmployee(employee);

        assertEquals(12, controller.getEmployeesList().size());
        assertTrue(employee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));

        String name="Violeta",newFunc="ASISTENT";

        Employee oldd = new Employee(name,"",DidacticFunction.ASISTENT,"");

        Employee neww = new Employee("","",DidacticFunction.valueOf(newFunc),"");
        controller.modifyEmployee(oldd,neww);
        assertEquals(controller.getEmployeesList().get(11).getFunction(),ASISTENT);
    }

    @Test
    public void test4() {

        Employee employee = new Employee("Mariana","1234567890123", LECTURER, "2000");
        assertTrue(employeeValidator.isValid(employee));
        controller.addEmployee(employee);

        assertEquals(13, controller.getEmployeesList().size());
        assertTrue(employee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));

        String name="Mariana",newFunc="TEACHER";

        Employee oldd = new Employee(name,"",DidacticFunction.ASISTENT,"");

        Employee neww = new Employee("","",DidacticFunction.valueOf(newFunc),"");
        controller.modifyEmployee(oldd,neww);
        assertEquals(controller.getEmployeesList().get(12).getFunction(),TEACHER);
    }


    @Test
    public void test5()  {

        Employee employee = new Employee("Alina","1234567890123", LECTURER, "3000");
        assertTrue(employeeValidator.isValid(employee));
        controller.addEmployee(employee);
        assertEquals(14, controller.getEmployeesList().size());
        assertTrue(employee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));

        String name="Alina",newFunc="ASISTENT";

        Employee oldd = new Employee(name,"",DidacticFunction.ASISTENT,"");

        Employee neww = new Employee("","",DidacticFunction.valueOf(newFunc),"");
        controller.modifyEmployee(oldd,neww);
        assertEquals(controller.getEmployeesList().get(13).getFunction(),ASISTENT);
    }

    @Test
    public void test6() {

       // Employee employee = new Employee("Nobody","1234567890123", LECTURER, "3000");
       // assertFalse(employeeValidator.isValid(employee));
      //  controller.addEmployee(employee);

        String name="Nobody",newFunc="ASISTENT";

        Employee oldd = new Employee(name,"",DidacticFunction.ASISTENT,"");

        Employee neww = new Employee("","",DidacticFunction.valueOf(newFunc),"");


//for the message it prints
        // Create a stream to hold the output
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        // IMPORTANT: Save the old System.out!
        PrintStream old = System.out;
        // Tell Java to use your special stream
        System.setOut(ps);
        controller.modifyEmployee(oldd,neww);
        // Put things back
        System.out.flush();
        System.setOut(old);
        ByteArrayOutputStream mesg = new ByteArrayOutputStream();

        //for the expected message
        PrintStream pss = new PrintStream(mesg);
        // IMPORTANT: Save the old System.out!
        PrintStream oold = System.out;
        // Tell Java to use your special stream
        System.setOut(pss);
        System.out.println("Angajatul cu acest nume nu exista.");
        System.out.flush();
        System.setOut(oold);

       assertEquals(mesg.toString(),baos.toString());
    }



}
